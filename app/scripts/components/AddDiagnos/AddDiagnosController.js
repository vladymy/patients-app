class AddDiagnosController  {

  /**
   * Constructor
   *
   * @param dataService
   * @param $state
   */
  constructor(dataService, $state ) {
    this.$state = $state;
    this.dataService = dataService;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
      dd='0'+dd
    }

    if(mm<10) {
      mm='0'+mm
    }

    today = mm+'/'+dd+'/'+yyyy;

    this.newDiagnos = {
      'code': '',
      'diagnosis': '',
      'startDate': today
    };
  }

  /**
   * Show the bottom sheet
   */
  addDiagnos(addDiagnosForm) {
    if(!addDiagnosForm.$invalid) {
      let $state = this.$state,
          dataService = this.dataService;

      this.patients.map((patient) => {
        if(patient.id === this.patient.id) {
          patient.currentDiagnoses = (patient.currentDiagnoses) ? patient.currentDiagnoses : [];
          this.newDiagnos.id = patient.currentDiagnoses.length + 1;
          patient.currentDiagnoses.push(this.newDiagnos);
        }
      });

      dataService.savePatients(this.patients);
      $state.go('patient', {patientId: this.patient.id})
    }
  }

}
export default AddDiagnosController;

