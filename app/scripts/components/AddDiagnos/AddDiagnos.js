import AddDiagnosController from './AddDiagnosController'

export default {
  name : 'addDiagnos',
  config : {
    bindings         : {  patients: '=', patient: '=' },
    templateUrl      : 'scripts/components/AddDiagnos/AddDiagnos.html',
    controller       : [ 'dataService', '$state', AddDiagnosController ]
  }
};