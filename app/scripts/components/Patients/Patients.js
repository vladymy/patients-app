export default {
  name : 'patients',
  config : {
    bindings         : { patients: '=' },
    templateUrl      : 'scripts/components/Patients/Patients.html'
  }
};
