class EditPatientInfoController  {

  /**
   * Constructor
   *
   * @param dataService
   * @param $state
   */
  constructor(dataService, $state) {
    this.$state = $state;
    this.dataService = dataService;

    this.newPatientInfo = {
      'name': this.patient.name,
      'birthday': this.patient.birthday,
      'address': this.patient.address
    };
  }

  updateInfo(updateInfoForm) {
    if(!updateInfoForm.$invalid) {
      let $state = this.$state,
        dataService = this.dataService;

      this.patients.map((patient) => {
        if (patient.id === this.patient.id) {
          patient.name = this.newPatientInfo.name;
          patient.birthday = this.newPatientInfo.birthday;
          patient.address = this.newPatientInfo.address;
        }
      });

      dataService.savePatients(this.patients);
      $state.go('patient', {patientId: this.patient.id})
    }
  }

}
export default EditPatientInfoController;

