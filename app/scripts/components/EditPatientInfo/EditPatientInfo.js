import EditPatientInfoController from './EditPatientInfoController'

export default {
  name : 'editPatientInfo',
  config : {
    bindings         : {  patients: '=', patient: '=' },
    templateUrl      : 'scripts/components/EditPatientInfo/EditPatientInfo.html',
    controller       : [ 'dataService', '$state', EditPatientInfoController ]
  }
};