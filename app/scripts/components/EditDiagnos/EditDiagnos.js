import EditDiagnosController from './EditDiagnosController'

export default {
  name : 'editDiagnos',
  config : {
    bindings         : {  patients: '=', patient: '=' },
    templateUrl      : 'scripts/components/EditDiagnos/EditDiagnos.html',
    controller       : [ 'dataService', '$state', 'localStorageService', EditDiagnosController ]
  }
};