class EditDiagnosController  {

  /**
   * Constructor
   *
   * @param $state
   * @param dataService
   */
  constructor(dataService, $state) {
    this.$state = $state;
    this.dataService = dataService;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
      dd='0'+dd
    }

    if(mm<10) {
      mm='0'+mm
    }

    today = mm+'/'+dd+'/'+yyyy;

    this.patient.currentDiagnoses.map((diagnos) => {
      if(diagnos.id == $state.params.diagnosId) {
        this.newDiagnos = {
          'code': diagnos.code,
          'diagnosis': diagnos.diagnosis,
          'startDate': today
        };
      }
    });

  }

  saveDiagnos(editDiagnosForm) {
    if(!editDiagnosForm.$invalid) {
      let $state = this.$state,
        dataService = this.dataService;

      this.patients.map((patient) => {
        if (patient.id === this.patient.id) {
          patient.currentDiagnoses.map((diagnos) => {
            if (diagnos.id == $state.params.diagnosId) {
              diagnos.code = this.newDiagnos.code;
              diagnos.diagnosis = this.newDiagnos.diagnosis;
            }
          });
        }
      });

      dataService.savePatients(this.patients);
      $state.go('patient', {patientId: this.patient.id})
    }
  }

}
export default EditDiagnosController;

