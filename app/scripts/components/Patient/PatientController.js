class PatientController  {

  /**
   * Constructor
   *
   * @param dataService
   */
  constructor(dataService) {
    this.dataService = dataService;
  }

  deleteDiagnos(diagnosId) {
    let dataService = this.dataService,
        today = new Date(),
        dd = today.getDate(),
        mm = today.getMonth()+1, //January is 0!
        yyyy = today.getFullYear();

    if(dd<10) {
      dd='0'+dd
    }

    if(mm<10) {
      mm='0'+mm
    }

    today = mm+'/'+dd+'/'+yyyy;

    this.patients.map((patient) => {
      if(patient.id === this.patient.id) {
        patient.diagnosesHistory = (patient.diagnosesHistory) ? patient.diagnosesHistory : [];
        patient.currentDiagnoses.map((diagnos, index) =>{
          if(diagnos.id == diagnosId) {
            diagnos.endDate = today;
            patient.diagnosesHistory.push(diagnos);
            patient.currentDiagnoses.splice(index, 1);
          }
        });
      }
    });
    dataService.savePatients(this.patients);
  }

}
export default PatientController;

