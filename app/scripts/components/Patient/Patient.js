import PatientController from './PatientController'

export default {
  name : 'patient',
  config : {
    bindings         : {  patient: '=', patients: '=' },
    templateUrl      : 'scripts/components/Patient/Patient.html',
    controller       : ['dataService', PatientController ]
  }
};