function AppController($state, $scope, dataService, localStorageService, $mdSidenav) {
  $scope.$state = $state;
  $scope.toggleSidebar   = toggleSidebar;

  function toggleSidebar()  {
    $mdSidenav('left').toggle();
  }

}

export default [ '$state', '$scope', 'dataService', 'localStorageService', '$mdSidenav', AppController ];
