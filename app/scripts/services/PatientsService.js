function PatientsService($q, $http, localStorageService) {
  var service,
      patients = JSON.parse(localStorageService.get('patients'));

  service = {
    getAllPatients: () => {
      if(patients && patients.length){
        return $q.when(patients);
      } else {
        return $http.get('data/patients.json', {cache: true}).then((resp) => {
          patients = resp.data;
          return patients;
        });
      }
    },

    getPatient: (id) => {
      function patientMatchesParam(patient) {
        return patient.id === id;
      }
      return service.getAllPatients().then((patients) => {
        return patients.find(patientMatchesParam)
      });
    },

    savePatients: (updatedPatients) => {
      patients = updatedPatients;
      localStorageService.set('patients', JSON.stringify(patients));
    }
  };
  return service;
}

export default ['$q', '$http', 'localStorageService', PatientsService];

