// Load libraries
import angular from 'angular';

import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-messages';
import 'angular-ui-router';
import 'angular-local-storage';

import AppController from './AppController';

import PatientsService from './services/PatientsService';

import AddDiagnos from './components/AddDiagnos/AddDiagnos';
import EditDiagnos from './components/EditDiagnos/EditDiagnos';
import EditPatientInfo from './components/EditPatientInfo/EditPatientInfo';
import Patients from './components/Patients/Patients';
import Patient from './components/Patient/Patient';

export default angular.module('patient-app', ['ngMaterial', 'ngMessages', 'ui.router', 'LocalStorageModule'])
  .config(($mdIconProvider, $mdThemingProvider, $stateProvider, localStorageServiceProvider, $urlRouterProvider) => {

    $urlRouterProvider.otherwise('/');

    let states = [
      {
        name: 'home',
        url: '/',
        component: 'patients',
        resolve: {
          patients: function (dataService) {
            return dataService.getAllPatients();
          }
        }
      }, {
        name: 'patient',
        url: '/patient/{patientId}',
        component: 'patient',
        resolve: {
          patients: function (dataService) {
            return dataService.getAllPatients();
          },
          patient: function (dataService, $transition$) {
            return dataService.getPatient($transition$.params().patientId);
          }
        }
      }, {
        name: 'addDiagnos',
        url: '/patient/{patientId}/addDiagnos',
        component: 'addDiagnos',
        resolve: {
          patients: function (dataService) {
            return dataService.getAllPatients();
          },
          patient: function (dataService, $transition$) {
            return dataService.getPatient($transition$.params().patientId);
          }
        }
      }, {
        name: 'editDiagnos',
        url: '/patient/{patientId}/editDiagnos/{diagnosId}',
        component: 'editDiagnos',
        resolve: {
          patients: function (dataService) {
            return dataService.getAllPatients();
          },
          patient: function (dataService, $transition$) {
            return dataService.getPatient($transition$.params().patientId);
          }
        }
      }, {
        name: 'editPatientInfo',
        url: '/patient/{patientId}/editPatientInfo',
        component: 'editPatientInfo',
        resolve: {
          patients: function (dataService) {
            return dataService.getAllPatients();
          },
          patient: function (dataService, $transition$) {
            return dataService.getPatient($transition$.params().patientId);
          }
        }
      }, {
        name: 'patients',
        url: '/patients',
        component: 'patients',
        resolve: {
          patients: function (dataService) {
            return dataService.getAllPatients();
          }
        }
      }, {
        name: 'about',
        url: '/about',
        template: '<h3>about world!</h3>'
      }
    ];

    // Loop over the state definitions and register them
    states.forEach(function (state) {
      $stateProvider.state(state);
    });

    localStorageServiceProvider
      .setPrefix('patientsApp');

    $mdThemingProvider.theme('default')
      .primaryPalette('blue',{

      })
      .accentPalette('blue-grey');
  })

  .component(EditPatientInfo.name, EditPatientInfo.config)
  .component(AddDiagnos.name, AddDiagnos.config)
  .component(EditDiagnos.name, EditDiagnos.config)

  .component(Patients.name, Patients.config)
  .component(Patient.name, Patient.config)

  .service('dataService', PatientsService)

  .controller('AppController', AppController);
